<a href="https://libreops.cc"><img src="https://libreops.cc/static/img/libreops.svg" width="300"></a>

Information about LibreOps Syncthing Relay.

## Syncthing Relay Stats

[Relay stats](https://relays.syncthing.net)

![LibreSync](LibreOps-SyncthingRelay.png "LibreOps Syncthing Relay")

## Relay ID

```bash
relay://192.71.166.93:22067/?id=JFGVNL2-UGGR7FP-ZFO6Q74-3KZ3OEY-D6YAWYS-QBQNK5O-TJMMM3C-ZUZTEQN
```

## Syncthing Client

```bash
Settings --> Connections --> Sync Protocol Listen Address
```

![syncthing](Syncthing-Client.png "Syncthing Client")

## Support

[![OpenCollective](https://img.shields.io/opencollective/all/libreops?color=%23800&label=opencollective&style=flat-square)](https://opencollective.com/libreops/)

## Join

[![irc](https://img.shields.io/badge/Matrix-%23libreops:matrix.org-blue.svg)](https://riot.im/app/#/room/#libreops:matrix.org)

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
